let assert = require('assert');
let fs = require('fs');
let dataEyes = {};
let dataGender = {};
let dataFriends = [];

describe('tdd lab', function() { 
    describe('#checkFileExist()', function() {
        it('should have homework5-1_eyes.json existed', function(done) {
            fs.access('homework5-1_eyes.json', fs.constants.R_OK | fs.constants.W_OK, (err) => { 
                if (err) {
                    done(err);
                } else {
                    done();
                }       
            });
        });
        it('should have homework5-1_gender.json existed', function(done) {
            fs.access('homework5-1_gender.json', fs.constants.R_OK | fs.constants.W_OK, (err) => { 
                if (err) {
                    done(err);
                } else {
                    done();
                }       
            });
        });
        it('should have homework5-1_friends.json existed', function(done) {
            fs.access('homework5-1_friends.json', fs.constants.R_OK | fs.constants.W_OK, (err) => { 
                if (err) {
                    done(err);
                } else {
                    done();
                }
            })
        });
    });
    describe('#objectKey()', function() {

        it('should have same object key stucture as homework5-1_eyes.json', function(done) {
            let dataExpect = {'brown':1, 'green':1, 'blue':1};
            let data = fs.readFile('./homework5-1_eyes.json', 'utf8', function(err, data){
                if (err)
                    done(err);
                else {
                    data = JSON.parse(data)
                    done();
                }
            }); 
        });
        it('should have same object key stucture as homework5-1_gender.json', function(done) {
            let dataExpect = {'female':1,'male':1};
            let data = fs.readFile('./homework5-1_eyes.json', 'utf8', function(err, data){
                if (err)
                    done(err);
                else {
                    data = JSON.parse(data)
                    done();
                }
            }); 
        });
        it('should have same object key stucture as homework5-1_friends.json', function() {
            let dataExpect = {'_id':'5a3711070776d02ed87d2100','friendCount':2};
            let data = fs.readFile('./homework5-1_eyes.json', 'utf8', function(err, data){
                if (err)
                    done(err);
                else {
                    data = JSON.parse(data)
                    data.map(element => {
                        assert.deepEqual(Object.keys(dataExpect),'should have same object key stucture as homework5-1_friends.json');
                    });
                    done();
                }                                    
            });
            
        });
    });
    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', function(done) {
            assert.equal(friends ,friends.length === 23, 'friends should have size of array input as 23')
            done();
        });
    });
    describe('#sumOfEyes()', function() {
        it('should have sum of eyes as 23', function(done) {
           let eyesAmount = Object.keys(dataEyes).map(function(k) {
               return dataEyes[k] 
            });
            eyesAmount = eyesAmount.reduce((sum ,amount) => {
                return sum + amount;
            }, 0);      
               assert.equal(eyesAmount, 23, 'should be sum of eyes as 23');
               done();
        });
    });
    describe('#sumOfGender()', function(done) {
        it('should have sum of gender as 23', function(done) {
            let  genderAmount = Object.keys(dataGender).map(function(k) {
                return genderGender[k]
            });
            genderAmount = genderAmount.reduce((sum, amount) => {
                return sum + amount;
            }, 0);
                assert.equal(genderAmount, 23, 'should have sum of gender as 23');
                done(); 
        });
    });
});


/**********************************/
let readData  = require('./readData');
let groupData = require('./groupData');
let countData = require('./countData');

async function start() {

    const data = await readData.readData('homework1-4.json');
    employees  = JSON.parse(data);

    dataEyes    = await groupData.groupData(employees, 'eyeColor');
    dataGender  = await groupData.groupData(employees, 'gender');
    dataFriends = await countData.countData(employees, 'friends');

    // console.log(dataEyes);
    // console.log(dataGender);
    // console.log(dataFriends);
}

start();
