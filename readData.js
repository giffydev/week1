let fs = require('fs');

function readData(filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(filename, 'utf8', function(err, data) {
            if (err) 
                reject(err);
            else             
                resolve(data);
        });
    });
}

exports.readData = readData;